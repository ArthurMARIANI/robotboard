import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-displaymeteo',
  templateUrl: './displaymeteo.component.html',
  styleUrls: ['./displaymeteo.component.css']
})
export class DisplaymeteoComponent implements OnInit {
  public message = 'Displaymeteomessage';
  public meteodatas = ['Marseille', 'Lyon', 'Paris'];
  constructor() { }

  ngOnInit() {
  }

}
